using System;
using System.IO;
using System.Threading.Tasks;


class MainClass
{
	static void Main()
	{
		checkProperties();
		streamBasicUsage();
		streamRW();
		smth();
	}
		
/////////////////////////////////////////////////////////////////////////////////////
		
//shows specific properties in stream class, using filestream
	static void checkProperties()
	{
	//check canRead/Write/Seek
		FileStream[] file = new FileStream[3];
		file[0] = new FileStream(@"file1", FileMode.OpenOrCreate, FileAccess.Read);
		file[1] = new FileStream(@"file2", FileMode.OpenOrCreate, FileAccess.Write);
		file[2] = new FileStream(@"file3", FileMode.OpenOrCreate, FileAccess.ReadWrite);
			
		for(int i=0; i<3; i++)
		{
			System.Console.WriteLine("File"+(i+1)+":");
			System.Console.WriteLine("Can read: "+file[i].CanRead);
			System.Console.WriteLine("Can write: "+file[i].CanWrite);
			System.Console.WriteLine("Can seek: "+file[i].CanSeek);
		}
			
		System.Console.WriteLine("OutputStream can seek: "+System.Console.OpenStandardOutput().CanSeek);
			
			
	//get full name
		System.Console.Write("\n\n");
		System.Console.WriteLine("file1 name: "+file[0].Name);
			
			
	//show Length
		//we can only get length of streams that can seek
		System.Console.Write("\n\n");
		System.Console.WriteLine("file2 length: "+file[1].Length);
		file[1].Write(new Byte[]{0x65, 0x66, 0x67}, 0, 3);
		System.Console.WriteLine("file2 length: "+file[1].Length);
		
		
	//position
		//also, it have to support seeking
		System.Console.Write("\n\n");
		System.Console.WriteLine("file2 position: "+file[1].Position);
		file[1].Seek(1, SeekOrigin.Begin);
		System.Console.WriteLine("file2 position: "+file[1].Position);
		
		foreach(FileStream x in file)
			x.Close();
	}
	
///////////////////////////////////////////////////////////////////////////////

//stream creation and usage
	async static void streamBasicUsage()
	{
	//create string
		FileStream file = new FileStream(@"file1", FileMode.Create);
		//File modes: Append; Create; CreateNew; Open; OpenOrCreate; Truncate
		
	//basic operation
		byte[] data = new byte[]{0x61, 0x62, 0x63, 0x64, 0x0a};
		byte[] data2= new byte[3];
		
		file.Write(data, 0, data.Length);
		//file.Read(data, 0, 3);

		file.Seek(0, SeekOrigin.Begin);
		
		int tmp = file.ReadByte();
		//file.WriteByte(Byte)
		System.Console.WriteLine(tmp);
		
		
		file.CopyTo(System.Console.OpenStandardOutput());
		//optionaly length as #2 argument
		
		
		file.Flush();//empty buffors

	//async operation
	
		IAsyncResult result = file.BeginWrite(data, 0, data.Length, new AsyncCallback(funCallback), null);
		//file.BeginRead(...)
		file.EndWrite(result);//wait till async operation compleate
		
		file.Seek(0, SeekOrigin.Begin);
		
		Task task = file.ReadAsync(data2, 0, 3);
		//file.WriteAsync(...)
		await task;//wait till end
		
		System.Console.WriteLine(string.Join(",", data2));

		//file.CopyToAsync(...);
	
		//file.FlushAsync()
		
	//lock stream
	
		file.Lock(2, 4);//lock bytes from - to
		file.Seek(2, SeekOrigin.Begin);
		file.WriteByte(0x70);
		file.Unlock(2, 4);

		
		file.Close();
	}
	
	static void funCallback(IAsyncResult result)
	{
		System.Console.WriteLine("finnished write");
	}

//////////////////////////////////////////////////////////////////////////////////////////

//stream writers and readers
	
	static void streamRW()
	{
		byte[] data = new byte[40];
		
		MemoryStream memStr = new MemoryStream(data);
		//MemoryStream(byte[], from, to)
		//MemoryStream(int) - resizeable
		
		
		StreamWriter strw = new StreamWriter(memStr);
		
		strw.Write("kartofle");
		strw.Write(56);
		strw.Write(' ');
		strw.Write(20.69);
		strw.Write(' ');
		
		strw.WriteLine("ziemniaki");
		strw.WriteLine(20);
		
		//strw.WriteAsync( char / char[] / string )
		//strw.WriteLineAsync( char / char[] / string )
		
		strw.Flush();
		memStr.Position = 0;
		
		StreamReader strr = new StreamReader(memStr);
		
		int tmp = strr.Peek();
		//strr.Read()
		
		System.Console.WriteLine((char) tmp);
		
		char[] tmpArray = new char[8];
		strr.Read(tmpArray, 0, tmpArray.Length);
		//strr.ReadAsync()
		System.Console.WriteLine(string.Join("", tmpArray));
		
		string tmpStr = strr.ReadLine();
		//strr.ReadLineAsync()
		System.Console.WriteLine(tmpStr);
		
		
		tmpStr = strr.ReadToEnd();
		//strr.ReadToEndAync()
		System.Console.WriteLine(tmpStr);
		
		
		memStr.Close();
		//strw.Close()
		//strr.Close()
	}

//////////////////////////////////////////////////////////////////////////

//basic file manage

	static void smth()
	{
	
		string tekst = "losowy napis";
		File.WriteAllText("file3", tekst);
		string plik = File.ReadAllText("file2");
		
		//File.Create("filename");
		File.Delete("file1");
		File.Delete("file2");
		File.Delete("file3");
		
		
	}
	
/////////////////////////////////////////////////////////////////////////
/*
System.IO.Stream
      Microsoft.JScript.COMCharStream - inner class, not for use
      System.Data.OracleClient.OracleBFile - oracle file type
      System.Data.OracleClient.OracleLob - --''--
      System.Data.SqlTypes.SqlFileStream - inteface with sql sqrver data
      System.IO.BufferedStream - additional buffer for stream
      System.IO.Compression.DeflateStream - stream with compression
      System.IO.Compression.GZipStream - --''--
      System.IO.FileStream
      System.IO.MemoryStream
      System.IO.Pipes.PipeStream
      System.IO.UnmanagedMemoryStream
      System.Net.Security.AuthenticatedStream - stream with autentication
      System.Net.Sockets.NetworkStream
      System.Printing.PrintQueueStream
      System.Security.Cryptography.CryptoStream - encrypted stream
      */
}
